﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pw_app.Models
{
    public class TransactionModels
    {
        [Key]
        public int Id { get; set; }
        public string RecipientId { get; set; }
        public virtual ApplicationUser Recipient { get; set; }
        public OperationType OperationType { get; set; }
        public double Value { get; set; }
    }

    public class TransactionHistoryModels
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public virtual TransactionModels Transaction { get; set; }
        public DateTime Created { get; set; }
        public double NewBalance { get; set; }
    }


    public class TransactionViewModel
    {
        public string UserId { get; set; } 
        public double Value { get; set; }
        public OperationType OperationType { get; set; }
    }

    public enum OperationType
    {
        Credited,
        Debited
    }

    public class UserSuggestModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}