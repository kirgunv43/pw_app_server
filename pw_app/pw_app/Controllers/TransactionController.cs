﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using pw_app.Models;

namespace pw_app.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/Transaction")]
    public class TransactionController : ApiController
    {

        [HttpPost]
        [Route("Create")]
        public async Task<IHttpActionResult> Create(TransactionViewModel model)
        {
            string id = User.Identity.GetUserId();
            ApplicationUser user = await Request.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(id);
            ApplicationUser recipient = await Request.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(model.UserId);
            if (model.OperationType == OperationType.Credited && model.Value < user.Balance)
            {
                return BadRequest(JsonConvert.SerializeObject(new { Message = "Пополните баланс для совершения данной операции" }));
            }

            if (recipient == null)
            {
                return BadRequest(JsonConvert.SerializeObject(new { Message = "Выберете адресата перевода" }));
            }

            TransactionHistoryModels transactionHistory = new TransactionHistoryModels()
            {
                Created = DateTime.Now,
                User = user,
                NewBalance = (model.OperationType == OperationType.Credited ? user.Balance - model.Value : user.Balance + model.Value),
                Transaction = new TransactionModels()
                {
                    OperationType = model.OperationType,
                    RecipientId = model.UserId,
                    Value = model.Value
                }
            };
            Request.GetOwinContext().Get<ApplicationDbContext>().TransactionHistory.Add(transactionHistory);
            Request.GetOwinContext().Get<ApplicationDbContext>().SaveChanges();
            return Ok();
        }
 
    }
}