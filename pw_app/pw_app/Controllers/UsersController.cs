﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using pw_app.Models;

namespace pw_app.Controllers
{
    [Authorize]
    [System.Web.Http.RoutePrefix("api/v1/Users")]
    public class UsersController : ApiController
    {
  
        [HttpGet]
        [Route("suggest")]
        public IHttpActionResult Suggest(string name)
        { 
            IEnumerable<UserSuggestModel> users =
                Request.GetOwinContext().GetUserManager<ApplicationUserManager>().Users.Where(item => item.FIO.Contains(name))
                    .Select(item => new UserSuggestModel {Name = item.FIO, Id = item.Id})
                    .Take(5).ToList();
            return Ok(JsonConvert.SerializeObject(users));
        }

        [HttpGet]
        [Route("balance")]
        public async Task<IHttpActionResult> GetBalance()
        {
            string id = User.Identity.GetUserId();
            ApplicationUser user = await Request.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(id);
            return Ok(JsonConvert.SerializeObject(new {balance = user?.Balance}));
        }
    }
}